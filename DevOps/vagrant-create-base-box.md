# Creating a Vagrant Base Box Image

The following is a minimal set of instructions to build a Vagrant base box image in VirtualBox.

This configuration is what you'll generally what you should expect when you download a box directly from the Vagrant Cloud. It is by no means secure and should absolutely not be used in a production environment.

By default, it uses a common username/password as well as an unsafe SSH key for secure shell access.

## 1. Configure the Virtual Machine

Ideally, a base box should contain the minimum configuration necessary to boot the OS. The following should be enough for any *nix based operating systems.

- Storage: 20GB; Type: Dynamically Allocated; Format: vmdk;
- Memory: 512MB
- Audio: Disabled
- USB: Disabled
- Networking: NAT
- Port Forwarding: SSH -> Host:2222; Guest: 2222

## 2. Install the OS

Set the virtual CD-ROM to boot the OS ISO.

Follow the OS installation instructions as if the OS were be installed to a physical hard drive.

When it prompts to set the username/password use the folowing:

- User: vagrant
- Pass: vagrant

## 3. Set the `root` Password

Log into the box for the first time. If logged into a GUI then launch the terminal

Set the root user password<sup>1</sup>

```sh
sudo passwd root
```

*<sup>1</sup> 'vagrant' is typically used as the default root password.*

## 4. Add the 'vagrant' User to Sudoers

The following will add 'vagrant' to the sudoers group. This allows vagrant to use the 'sudo' command without a password

Login as the 'root' user

```sh
sudo su -
```

Edit the sudoers file for 'vagrant'

```sh
sudo visudo -f /etc/sudoers.d/vagrant
```

This should open the sudoers config for 'vagrant' in `vi`. To edit the file, change to 'insert' mode by pressing 'i' and add the following:

```sh
vagrant ALL=(ALL) NOPASSWD:ALL
```

To save, exit and write to file with the following command

```sh
ESC + wq! + ENTER
```

To verify that it works try printing the sudoers file using `cat`

```sh
sudo cat /etc/sudoers.d/vagrant
```

If it prints the contents of the file to the console without asking for a password, it worked

## 5. Update The OS

Update the sources

```sh
sudo apt-get update -y
```

Upgrade all applications

```sh
sudo apt-get upgrade -y
```

Restart the VM

```sh
shutdown -r now
```

## 6. Enable and Configure the SSH daemon

SSH (Secure Shell) is required to login to the machine via `vagrant ssh` as well as provisioning the VM using Puppet and/or shell scripts.

First check if SSHd is already installed and/or running

```sh
sudo service --status-all | grep ssh
```

If there is no output install the SSH service

```sh
sudo apt-get install openssh-server
```

If the service is visible but has a `-` symbol within the brackets. Ex:

```sh
[-] ssh
```

The service can be enabled with the following

```sh
sudo update-rc.d minidlna enable
```

Now that we can guarantee the SSH service is both installed and enabled, open the config

```sh
sudo nano /etc/ssh/sshd_conf
```

Uncomment the line containing the following setting to enable it

```sh
AuthorizedKeysFile .ssh/authorized_keys
```

Save and exit

To enable the new setting restart the server

```sh
sudo service ssh restart
```

## 7. Install VirtualBox Guest Utilities

Guest utilities enables better Host <-> Guest interoperability. This is specifically useful for copy-pasting commands into the Guest OS.

To install Guest Tools, load a virtual CD-ROM with the `VBoxGuestAdditions.iso`. This should be included when you installed VirtualBox. Newer versions can also be downloaded from the web.

First, mount the virtual CD

```sh
sudo mount /dev/cdrom /mnt 
```

Change to the directory where the cd was mounted

```sh
cd /mnt
```

Run the guest additions install script

```sh
sudo ./VBoxLinuxAdditions.run
```

Finally, restart the VM

```sh
shutdown -r now
```

Once the VM comes back up you should be able to use/enable the GuestOS supported features.

## 8. Add the 'vagrant' Unsafe Private Key

The following will download and install a common SSH key that vagrant uses to login to VMs. In more recent versions of Vagrant, this key will be detected and automatically replaced with a safer key on the first 'vagrant up'.

Create and own the .ssh folder in the 'vagrant' user's home directory

```sh
mkdir -p /home/vagrant/.ssh
chmod 0700 /home/vagrant/.ssh
```

Download the key and append it to `~/.ssh/authorized_keys`

```sh
wget --no-check-certificate \
    https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub \
    -O /home/vagrant/.ssh/authorized_keys
```

Take ownership of `authorized_keys` and enable read+write permission

```sh
chmod 0600 /home/vagrant/.ssh/authorized_keys && \
chown -R vagrant /home/vagrant/.ssh
```

## 9. Package the Vagrant Box

Prior to packaging, it's a good idea to optimize the virtual disk (ie .vmdk) to reduce its size. The following zeroes out any unused data on the disk so it can be easily removed

```sh
sudo dd if=/dev/zero of=/EMPTY bs=1M && \
sudo rm -f /EMPTY
```

Once that is complete, shutdown the VM and change to a directory where you want the `.box` file saved.

Save the box with 

```sh
vagrant package --base [vm-name]
```

*Note: Replace `[vm-name]` with the name of the VirtualBox VM you want packaged.*

The output of the previous command should be a `package.box` file that can be used directly with Vagrant.


## 10. Test the image locally

To test the image we first need to name the box and add it to vagrant's local repository of boxes.

```sh
vagrant box add [box-name] package.box
vagrant init [box-name]
vagrant up
```

*Note: Replace `[box-name]` with a name of your choosing.*

This will generage a `Vagrantfile` containing a base configuration for the box as well as a bunch of commented-out instructions.

You may have some issues with `vagrant up` if the OS boots by default into a GUI shell. To fix this add the following option.

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "[box-name]"

  # Add this Block {
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
  end
  # }

end
```

## Appendix A: Upload the Box to Vagrant Cloud

Now that you've gone through all the work of creating a Box. It may be useful to make it availble to others.

1. Create an account at `https://app.vagrantup.com/`

2. Create a Box

Once logged in goto the `Dashboard` and click the `New Vagrant Box` button.

Give the box a Name and a brief Description

3. Set the Version

Set the Version to 1.0 and add a description outlining the steps used to create the box. For Example:
```
Minimal install.

Includes:
- password-less sudo
- no-config ssh 
- VBoxGuestAdditions 5.2.8

Login:
- u: vagrant
- p: vagrant
```

4. Set the Provider

The provider specifies what type of virtual machine the Box image was created on. Set the provider to `virtualbox`.

On this page you can also specify either an external URL where the box will be hosted (ex Amazon S3) or you can upload the box image directly to the Vagrant Cloud.

*Note: Vagrant supports many virtual machine formats and each box specifier may contain Box packages for more than one box format. Generally `virtualbox` is the most common format.` 

5. Release the Box

Once that is done, the box is uploaded but it won't show up under the Vagrant Cloud search. To make it public, goto `Versions` and select `Release` next to the 1.0 version.

-----

That's it! Congratulations. You have officially published your first Vagrant Box.

If you'd like to use it in the future just specify the following in a `Vagrantfile`.

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "[vcloud-username]/[box-name]"
end
```

Then call

```sh
vagrant up
```

Vagrant will download the latest box version to your local collection of boxes and boot up the virtual machine.

## Appendix B: Additional Resouces

*Sources:*

- [Building a Vagrant Box - Engine Yard](https://www.engineyard.com/blog/building-a-vagrant-box)